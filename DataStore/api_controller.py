import requests


class ApiController:
    def __init__(self):
        self.base_url = "http://localhost:8000/api/"

    def select_matching_user(self, first_name, surname):
        function_url = self.base_url + 'select_matching_user'
        params = {'first_name': first_name, 'surname': surname}
        response = requests.post(
            url=function_url,
            data=params
        )
        results = response.json()
        matches = []
        for match in results['results']:
            matches.append([match['firstName'], match['surname']])

        return matches

    def add_user(self, first_name, surname, age):
        function_url = self.base_url + 'add_user'
        params = {'first_name': first_name, 'surname': surname, 'age': age}
        requests.post(
            url=function_url,
            data=params
        )

    def authorised_user(self, username, password):
        function_url = self.base_url + 'authorised_user'
        params = {'username': username, 'password': password}
        response = requests.post(
            url=function_url,
            data=params
        )
        results = response.json()

        return results['results'][0]['Found']
