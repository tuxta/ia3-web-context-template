# IA2-web_context-template



## What is the purpose of this template?

This template is for teachers and students in the 
Queensland Digital Solutions Senior General subject 
that have selected the Web context for IA3

It is customised for the requirements of the 
assessment, and doesn't necessarily follow
industry practices. An example of this is
making http requests without a try-except
block to check for connection errors.
For IA3, students only need to make the 
connection, and using OurAPI we can ensure
the connection is available.

## Why use Flask instead of Bottle, FastAPI, Django etc?

I selected Flask, but Bottle and FastAPI would also have been
perfectly good options. I had to pick something, so I went
with the one that seems to be the most popular with QLD teachers.

Django for me is the framework I would personally use as an
experienced developer, but it has too much of a learning
curve to make it a viable choice for this assignment (IMHO).

## Usage
Just open the project up in your favorite editor (PyCharm and VSCode/Codium are good choices)
and then run the main.py file. That's it!! no need for running daemons/services such as Apache
and MySQL, just run with Python and life is grand. This is everything you need for IA3

## Login details
The only user in the database is
username: Harry
password: securepassword

The password is kept in plain text in the database, this allows for a teaching
moment and the students of course will apply a hashing algorithm (SHA2 I imagine)
for their implementation.

## Authors and acknowledgment
Written by Steven Tucker and licensed so that all can use, modify and share

## License
GPL V3
