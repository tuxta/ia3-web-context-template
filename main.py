from flask import Flask, render_template, redirect, url_for, session
from DataStore import ApiController

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mxzcoiqwenv89vsdvnq0vadf23ndsfgq03w4'


@app.route("/login", methods=['GET', 'POST'])
def login():
    from Forms import LoginDetails
    api = ApiController()
    form = LoginDetails()
    message = ""
    if form.is_submitted():
        username = form.username.data
        password = form.password.data
        valid_user = api.authorised_user(username, password)
        print(f"Valid user: {valid_user}")
        if valid_user:
            session['user_token'] = '5463'
            return redirect('/')
        else:
            message = "Invalid Login"
    return render_template('login.html', form=form, message=message)


@app.route("/logout")
def logout():
    session.pop('user_token')
    return redirect(url_for('login'))


@app.route("/")
def index():
    if session.get('user_token') != '5463':
        return redirect(url_for('login'))
    return render_template("index.html")


@app.route("/user_search", methods=['GET', 'POST'])
def search_users():
    if session.get('user_token') != '5463':
        return redirect(url_for('login'))
    from Forms import UserDetails
    api = ApiController()
    form = UserDetails()
    message = ""
    if form.is_submitted():
        first_name = form.first_name.data
        surname = form.surname.data
        results = api.select_matching_user(first_name, surname)
        if len(results) > 0:
            session['matched_users'] = results
            return redirect(url_for('user_details'))
        else:
            message = "No one in our database matches that search"
    return render_template('users_search.html', form=form, message=message)


@app.route("/user_details", methods=['GET'])
def user_details():
    if session.get('user_token') != '5463':
        return redirect(url_for('login'))
    return render_template('users_data.html')


@app.route("/add_user", methods=['GET', 'POST'])
def add_user():
    if session.get('user_token') != '5463':
        return redirect(url_for('login'))
    from Forms import AddUser
    api = ApiController()
    form = AddUser()
    if form.is_submitted():
        api.add_user(form.first_name.data, form.surname.data, form.age.data)
        return redirect(url_for('index'))

    return render_template("add_user.html", form=form)


@app.route("/data")
def data():
    if session.get('user_token') != '5463':
        return redirect(url_for('login'))
    return render_template("data.html")


@app.errorhandler(404)
def not_found_error(e):
    return render_template("not-found.html")


if __name__ == '__main__':
    app.run(host="127.0.0.1", port=8080, debug=True)
